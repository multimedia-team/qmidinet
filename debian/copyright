Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: QmidiNet
Upstream-Contact: Rui Nuno Capela <rncbc@rncbc.org>
Source: https://sourceforge.net/projects/qmidinet/files/

Files: *
Copyright: 2010-2024 Rui Nuno Capela <rncbc@rncbc.org>
License: GPL-2+

Files: src/appdata/*
Copyright: 2010-2022 Rui Nuno Capela
License: FSFAP
 Copying and distribution of this file, with or without modification,
 are permitted in any medium without royalty provided the copyright
 notice and this notice are preserved. This file is offered as-is,
 without any warranty.

Files: debian/*
Copyright:
 2010-2014 Alessio Treglia <alessio@debian.org>
 2014-2017 Jaromír Mikeš <mira.mikes@seznam.cz>
 2020-2024 Dennis Braun <snd@debian.org>
License: GPL-2+

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License can be found in `/usr/share/common-licenses/GPL-2'.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
